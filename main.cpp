#include <iostream>

#include <string>

class TestReview {
public:
  int name1;
  std::string name2;
  std::string name3;
};

std::string hello(std::string name) {
  using namespace std::string_literals;
  return "Hello "s + name + "!\n"s;
}

int main() { std::cout << hello("World"); }
