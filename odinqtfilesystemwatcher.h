#ifndef ODINQTFILESYSTEMWATCHER_H
#define ODINQTFILESYSTEMWATCHER_H

#include <odin-qt-file/odinqtfile_global.h>

#include <QFileSystemWatcher>

#include <cmath>

class OdinQtFileSystemWatcher : public QObject {
    Q_OBJECT
public:
    explicit OdinQtFileSystemWatcher( const QStringList& paths, QObject* parent = nullptr );
private:
    QFileSystemWatcher _watcher;
};

#endif // ODINQTFILESYSTEMWATCHER_H